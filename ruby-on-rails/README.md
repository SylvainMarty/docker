# Ruby on rails APP

## 1) Bootstrap a new Rails application
```sh
docker run -it --rm \
          --user "$(id -u):$(id -g)" \
          -v "$PWD":/usr/src/app \
          -w /usr/src/app \
          rails rails new \
          --skip-bundle webapp
```

## 2) Generate a Gemfile.lock
```sh
docker run --rm \
      -v "$PWD":/usr/src/app \
      -w /usr/src/app \
      ruby:2.1 \
      bundle install
```

## 3) Create the Dockerfile
```sh
vim Dockerfile
```
Paste this : `FROM rails:onbuild`

## 4) Build the container
```sh
docker build -t r-on-rails-app .
```

## 5) Launch the ruby-on-rails container
```sh
docker run --name rails-app -p 8080:3000 -d r-on-rails-app
```

# Plesk 12.5 - Docker - Ubuntu 14.04 - kernel 3.13.0

ressource : https://fr.ikoula.wiki/fr/Comment_d%C3%A9ployer_Plesk_dans_un_conteneur

## Installer Plesk 12.5

Installer le container :
<pre>
docker run --privileged -d -it -p 21:21 -p 80:80 -p 443:443 -p 8880:8880 plesk/plesk:12.5
</pre>

On peut aussi laisser Docker choisir la dernière version :
<pre>
docker run --privileged -d -it -p 21:21 -p 80:80 -p 443:443 -p 8880:8880 plesk/plesk
</pre>

Pour avoir tous les ports pris en compte :
<pre>
docker run --privileged -d -it -p 20:20 -p 21:21 -p 80:80 -p 443:443 -p 8880:8880 -p 8443:8443 -p 8447:8447 plesk/plesk
</pre>

Rajouter <code>--privileged</code> à la commande `docker run` pour éviter l'erreur 502.

Accéder à l'interface avec http://domain.tld:8880 ou avec https://domain.tld:8443


-------------------------------------------------------------


## Réparer l'erreur 502 Bad gateway - nginx

### D'abord, enlever complètement app_armor :

ressource : http://www.ehowstuff.com/how-to-disable-and-remove-apparmor-on-ubuntu-14-04/

Vérifier que app_armor existe :
<pre>
sudo apparmor_status
</pre>

Arrêter app_armor :
<pre>
sudo /etc/init.d/apparmor stop
sudo update-rc.d -f apparmor remove
</pre>

Désinstallation du logiciel :
<pre>
sudo apt-get remove apparmor apparmor-utils -y
</pre>

Redémarrer le serveur :
<pre>
sudo reboot
</pre>

### Réparation de Plesk 12.5 dans le container

Chercher l'id du container :
<pre>
docker ps -a
</pre>

Rentrer dans le bash du container :
<pre>
docker exec -it [container_id] bash
</pre>

Lancer la commande de réparation :
<pre>
plesk repair all -y
</pre>

Vérifier que l'admin web de Plesk fonctionne.
source installation docker : https://docs.docker.com/engine/installation/ubuntulinux/


-------------------------------------------------------------


# Pour nettoyer les containers inactifs :
-------

```sh
docker rm `docker ps -aq`
```


# Installer docker

## 1/ ajout cle
<pre>sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D</pre>

## 2/ ajout sources apt
<pre>sudo sh -c "echo 'deb https://apt.dockerproject.org/repo ubuntu-trusty main' > /etc/apt/sources.list.d/docker.list"</pre>

## 3/ installer docker
<pre>
sudo apt-get update && sudo apt-get purge lxc-docker ;
sudo apt-cache policy docker-engine
</pre>

Facultatif :
<pre>
sudo apt-get install linux-image-extra-$(uname -r)
</pre>

On finit par :
<pre>
sudo apt-get install docker-engine
</pre>

## 4/ premier démarrage
<pre>
sudo service docker start
sudo docker run hello-world
</pre>

## 5/ se reconnecter pour que les changements de groupes prennent effet
<pre>
exit
</pre>


-------------------------------------------------------------


# Changer de Kernel sur Ubuntu 14.04 Kernel 2.6.32 (demandé par Docker)

Après avoir installé un nouveau kernel :
<pre>
sudo apt-get update && sudo apt-get dist-upgrade && sudo apt-get autoremove
apt-get install linux-image-extra-virtual
</pre>

Ensuite :
<pre>
aptitude purge grub
aptitude install grub
update-grub
shutdown -r -f now
</pre>

Au redémarrage, installer https API :
<pre>
apt-get install apt-transport-https
</pre>


-------------------------------------------------------------


# Modifier les ports d'un container existant

Arrêter le container :
<pre>
docker ps -a
docker stop [container_id]
</pre>

Commit du container :
<pre>
docker commit [old_container_id] [new_container_id]
</pre>

Arrêter le container :
<pre>
docker stop [container_id]
</pre>

Arrêter le container :
<pre>
docker run -p port:port [...mapping...] -td [container_id]
</pre>


-------------------------------------------------------------


# COMMANDES DOCKER principales

manipuler les images

<pre>
docker pull
docker images
docker rmi
manipuler les docker
</pre>

<pre>
docker run
docker start
docker stop
docker rm
docker ps
docker ps -a
docker exec -it [container_name ou id] /bin/bash || docker exec -it [container_name ou id] bash
</pre>


-------------------------------------------------------------


# DOCKER SFTP : https://hub.docker.com/r/atmoz/sftp/

<pre>
docker pull atmoz/sftp
mkdir $HOME/docker-sftp-conf
cd $HOME/docker-sftp-conf/
vim users.conf
</pre>

Dans users.conf insérer les lignes avec login:mot de passe:id
comme ceci :
<pre>
user1:pass1:1001
user2:pass2:1002
</pre>

<pre>
docker run --name sftp \
    -v $HOME/docker-sftp-conf/users.conf:/etc/sftp-users.conf:ro \
    -v /var/www/html/:/home/user1/share \
    -v /var/www/html/:/home/user2/share \
    -p 2222:22 -d atmoz/sftp
</pre>
# MySQL Server

## 1)
```sh
docker run --name mysql-server -e MYSQL_ROOT_PASSWORD={{MOT DE PASSE}} -e MYSQL_DATABASE={{NOM BDD}} -e MYSQL_USER={{NOM D'UTILISATEUR}} -e MYSQL_PASSWORD={{MOT DE PASSE}} -p 3306:3306 -v {{PATH TO}}:/var/lib/mysql -d mysql:latest --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
```

## 2)
```sh
docker run -it --link mysql-server:mysql --rm mysql sh -c 'exec mysql -h"$MYSQL_PORT_3306_TCP_ADDR" -P"$MYSQL_PORT_3306_TCP_PORT" -uroot -p"$MYSQL_ENV_MYSQL_ROOT_PASSWORD"'
```
# Build
```sh
docker build -t ubuntu-workspace .
```

# Run
```sh
docker run -d --name antoine-workspace -p 2220:22 ubuntu-workspace
```

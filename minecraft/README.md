# Lancer un serveur minecraft via Docker


### Fonctionne de cette façon :
```sh
docker run -d -v ~/[DOSSIER DE L\'HÔTE]:/data -e EULA=TRUE -e OPS=[IdJoueur1],[IdJoueur1] -e WHITELIST=[IdJoueur1],[IdJoueur1] -e LEVEL_TYPE=DEFAULT -p 25565:25565 --name mc itzg/minecraft-server
```

Pour plus d'infos, voir le docker de [ITZG](https://hub.docker.com/r/itzg/minecraft-server/) 

### Exemple ligne de commande qui fonctionne :
```sh
docker run -d -v ~/docker-minecraft-config:/data -e EULA=TRUE -e OPS=Skykiller007,MrDelicatesse -e WHITELIST=Skykiller007,MrDelicatesse -e LEVEL_TYPE=DEFAULT -p 25565:25565 --name mc itzg/minecraft-server
```
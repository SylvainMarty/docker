Pour ajouter un vhost Apache2 et PHP
```sh
docker run -d --name apache-server -v ~/html/htdocs:/usr/local/apache2/htdocs/ -p 80:80 httpd:2.4
```

Sur le linux maison :
```sh
docker run -d --name apache-server -v ~/html/htdocs:/usr/local/apache2/htdocs/ -p 80:80 apache-server-vhosts
```

Voir ce [site](http://fr.jeffprod.com/blog/2015/lamp-sous-docker.html) pour les explications
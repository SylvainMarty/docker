# Mise en place d'un docker Serveur FTP
Source : [stilliard/docker-pure-ftpd](https://github.com/stilliard/docker-pure-ftpd)

## Mise en place du Docker :
```sh
docker pull stilliard/pure-ftpd:hardened

docker run -d --name ftpd-server -p 2121:21 -p 30000-30009:30000-30009 -e "PUBLICHOST=localhost" -v ~/vhosts:/home/ftpusers/sylvain stilliard/pure-ftpd:hardened
```

Sachant que le dossier `~/vhosts` correspond au dossier contenant mes fichiers que je veux rendre accessible via FTP.
On peut aussi coupler cette commande avec `--restart ALWAYS` afin que le container redémarre au démarrage de Docker.

------

## Pour ajouter des utilisateurs :

Connexion au bash du container :
```sh
docker exec -it ftpd-server /bin/bash
```

Création d'un utilisateur :
```sh
pure-pw useradd sylvain -u ftpuser -d /home/ftpusers/sylvain
```
A l'éxécution de la commande, le container va proposer d'entrer un mot de passe pour cet utilisateur.

Flushing des utilisateurs :
```sh
pure-pw mkdb
```

-----

## Exemple d'utilisation

Sur le navigateur :
`ftp://linux.itsmyskills.com:2121/`
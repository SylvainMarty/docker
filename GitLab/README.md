# Installer Gitlab : http://doc.gitlab.com/omnibus/docker/README.html

Lancer cette commande docker :
<pre>
docker run gitlab/gitlab-ce
</pre>

OU :

<pre>
sudo docker run --detach \
	    --hostname gitlab.example.com \
	    --env GITLAB_OMNIBUS_CONFIG="external_url 'http://my.domain.com/'; gitlab_rails['lfs_enabled'] = true;" \
	    --publish 443:443 --publish 80:80 --publish 22:22 \
	    --name gitlab \
	    --restart always \
	    --volume /srv/gitlab/config:/etc/gitlab \
	    --volume /srv/gitlab/logs:/var/log/gitlab \
	    --volume /srv/gitlab/data:/var/opt/gitlab \
	    gitlab/gitlab-ce:latest
</pre>


-------------------------------------------------------------


## Exemple :

<pre>
sudo docker run --detach \
	    --hostname lab.itsmyskills.com \
	    --env GITLAB_OMNIBUS_CONFIG="external_url 'http://lab.itsmyskills.com/'; gitlab_rails['lfs_enabled'] = true;" \
	    --publish 1443:443 --publish 1080:80 --publish 1022:22 \
	    --name gitlab \
	    --restart always \
	    --volume /srv/gitlab/config:/etc/gitlab \
	    --volume /srv/gitlab/logs:/var/log/gitlab \
	    --volume /srv/gitlab/data:/var/opt/gitlab \
	    gitlab/gitlab-ce:latest
</pre>

# GitLab CI Runner
(https://gitlab.com/help/ci%2Fquick_start/README)

(https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/install/docker.md)

(https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/executors/docker.md#define-image-and-services-from-gitlab-ci-yml)

(https://www.stefanwienert.de/blog/2015/11/07/gitlab-ci-with-docker-starting-guide/)

(http://docs.gitlab.com/ee/integration/jenkins.html)